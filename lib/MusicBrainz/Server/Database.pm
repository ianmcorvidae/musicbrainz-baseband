package MusicBrainz::Server::Database;
use Moo;

has 'username' => (
    is  => 'rw',
);

has 'schema' => (
    is  => 'rw',
);

has 'password' => (
    is  => 'rw',
);

has 'database' => (
    is  => 'rw',
);

has 'host' => (
    is  => 'rw',
);

has 'port' => (
    is  => 'rw'
);

sub shell_args
{
    my $self = shift;
    my @args;

    my %vars = (
        '-h' => $self->host,
        '-p' => $self->port,
        '-U' => $self->username
    );

    push @args, map { $_ => $vars{$_} } grep { $vars{$_} } keys %vars;

    push @args, $self->database;

    if (wantarray) {
        return @args;
    }
    else {
        require String::ShellQuote;
        return join " ", map { String::ShellQuote::shell_quote($_) } @args;
    }
}

1;
